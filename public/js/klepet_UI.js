/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var besedilo;
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    besedilo = divElementHtmlTekst(sporocilo);
  } else {
    besedilo = $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
  sporocilo = sporocilo.replace("https://sandbox.lavbic.net/teaching/OIS/gradivo/","");
  if (sporocilo.match(/\b(http\S+\.(?:jpg|png|gif))\b/g)) {
    var slike = sporocilo.match(/\b(http\S+\.(?:jpg|png|gif))\b/g);
    for (var i=0; i<slike.length; i++) {
      besedilo.append( '<br /><img src="'+slike[i]+'" width="200" style="margin-left:20px">' );
    }
  }
  return besedilo;
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  var besedilo = $('<div></div>').html('<i>' + sporocilo + '</i>')
  sporocilo = sporocilo.replace("https://sandbox.lavbic.net/teaching/OIS/gradivo/","");
  if (sporocilo.match(/\b(http\S+\.(?:jpg|png|gif))\b/g)) {
    var slike = sporocilo.match(/\b(http\S+\.(?:jpg|png|gif))\b/g);
    for (var i=0; i<slike.length; i++) {
      besedilo.append( '<br /><img src="'+slike[i]+'" width="200" style="margin-left:20px">' );
    }
  }
  return besedilo;
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";
var nadimki = {};

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var uporabnik = (nadimki[sporocilo.id]) ? nadimki[sporocilo.id]+" ("+sporocilo.vzdevek+")" : sporocilo.vzdevek;
    var tekst = uporabnik ? uporabnik+sporocilo.besedilo : sporocilo.besedilo;
    var novElement = divElementEnostavniTekst(tekst);
    $('#sporocila').append(novElement);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki, id) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      var tekst = (nadimki[id[i]]) ? nadimki[id[i]]+" ("+uporabniki[i]+")" : uporabniki[i];
      $('#seznam-uporabnikov').append(
        divElementEnostavniTekst(tekst).data({
          id: id[i],
          vzdevek: uporabniki[i]
        })
      );
    }
    
    // S klikom na vzevek pošljemo uporabniku simbol ☜ preko zasebnega sporočila
    $('#seznam-uporabnikov div').click(function() {
      var sporocilo = '/zasebno "'+$(this).data("vzdevek")+'" "☜"';
      var sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
      if (sistemskoSporocilo) {
        $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
      }
    });
  });
  
  socket.on('vrniIdVzdevka', function(uporabnik){
    if(uporabnik.id){
      var id = uporabnik.id;
      if (!uporabnik.nadimek) delete nadimki[id];
      else nadimki[id] = uporabnik.nadimek;
    }
  });
  
  socket.on('zasebno', function(sporocilo){
    var id = sporocilo.id;
    var besedilo = (nadimki[sporocilo.id]) ? "(zasebno za "+nadimki[sporocilo.id]+" ("+sporocilo.vzdevek+"))" : "(zasebno za "+sporocilo.vzdevek+")";
    besedilo+=": "+sporocilo.besedilo;
     $('#sporocila').append(divElementHtmlTekst(besedilo));
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
